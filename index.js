var express = require('express');  
var bodyParser = require('body-parser');  
var request = require('request');  
var app = express();

// constantes de verificação de payload
const PAYLOAD_RANDOM_IMGUR = 0;
const PAYLOAD_MEME_IMGUR = 1;
const PAYLOAD_RANDOM_XKCD = 2;
const PAYLOAD_RANDOM_KONACHAN = 3;
const PAYLOAD_HELP = 4;
const PAYLOAD_MENU = 5;

app.use(bodyParser.urlencoded({extended: false}));  
app.use(bodyParser.json());  
app.listen((process.env.PORT || 3000));

app.get('/', function (req, res) {  
	res.send('Um servidor de bots de teste!');
});

app.get('/webhook', function(req, res) {
	if (req.query['hub.mode']         === 'subscribe' &&
		req.query['hub.verify_token'] === 'estagio_teste') {
	    
	    console.log("Validando webhook...");
	    res.status(200).send(req.query['hub.challenge']);
	} 
	else {
		console.error("Falha de verificação.");
	    res.sendStatus(403);          
	}  
});

app.post('/webhook', function (req, res) {
	var data = req.body;

	if (data.object === 'page') {
    
		data.entry.forEach(function(entry) {

			entry.messaging.forEach(function(event) {

				if (event.message) {
					sendTemplate(event.sender.id);
				}

				else if(event.postback){
					
					switch(Number(event.postback.payload)){
						case PAYLOAD_RANDOM_IMGUR:
							fetchFromImgur(event.sender.id, PAYLOAD_RANDOM_IMGUR);
							break;

						case PAYLOAD_MEME_IMGUR:
							fetchFromImgur(event.sender.id, PAYLOAD_MEME_IMGUR);
							break;

						case PAYLOAD_RANDOM_XKCD:
							fetchFromXkcd(event.sender.id);
							break;

						case PAYLOAD_RANDOM_KONACHAN:
							fetchFromKonachan(event.sender.id);
							break;

						case PAYLOAD_HELP:
							helpMessage(event.sender.id);
							break;

						case PAYLOAD_MENU:
							sendTemplate(event.sender.id);
							break;

						default:
							sendTextMessage(event.sender.id, "Entrou no Default");

					}
				} 
				else {
					console.log("Evento Desconhecido ", event);
				}
			});
		
		});
		
		res.sendStatus(200);
	}
});



function fetchFromXkcd(recipientId){
	const random_number = Math.floor((Math.random() * 1815) + 1);
	request({
		uri: 'http://xkcd.com/'+ random_number.toString() +'/info.0.json',
		method: 'GET'
	},
	function (error, responde, body){
		if(!error){
			var json = JSON.parse(body);
			var image_url = json.img;
			sendImageMessage(recipientId, image_url, PAYLOAD_RANDOM_XKCD);
		}
		else{
			console.error("Erro XKCD");
			console.error(response);
		}
	});

}

function fetchFromImgur(recipientId, type_payload){
	
	// ID recebido ao se cadastrar na plataforma de desenvolvimento do Imgur
	// Necessário pra autorização de acesso aos dados públicos da plataforma
	const CLIENT_ID = 'Client-ID 9774e971e18b818';
	var page = Math.floor((Math.random() * 48) + 1);
	var url = 'https://api.imgur.com/3/gallery/random/random/';
	
	if (type_payload == PAYLOAD_MEME_IMGUR){
		url = 'https://api.imgur.com/3/g/memes/viral/';
	}

	request({
		uri: url + page.toString(),
		method: 'GET',
		headers: { Authorization: CLIENT_ID, Accept: 'application/json'}
	},
	function(error, response, body){
		if(!error){
			var json = JSON.parse(body);
			var image_url = json.data[page].link;

			// Garantir que uma imagem foi escolhida da lista retornada pelo Imgur
			while(!image_url.includes('.jpg') && !image_url.includes('.jpeg') && !image_url.includes('.gif') && !image_url.includes('.png')){
				page = Math.floor((Math.random() * 10));
				image_url = json.data[page].link;
			}
			
			sendImageMessage(recipientId, image_url, type_payload);
			
		}
		else{
			console.error("Erro Imgur!");
			console.error(response);
		}
	});

}

function fetchFromKonachan(recipientId){
	const limit = 200;
	const url = 'http://konachan.net/post.json?limit=' + limit.toString();
	var post = Math.floor((Math.random() * limit));
	
	request({
		uri: url,
		method: 'GET'
	},
	function(error, response, body){
		if(!error){
			var json = JSON.parse(body);

			// Garantir que a imagem é safe!
			while(json[post].rating != 's'){
				post = Math.floor((Math.random() * limit));
			}
			// Por algum motivo a property de URL não possui início http:
			var image_url = 'https:' + json[post].file_url;

			sendImageMessage(recipientId, image_url, PAYLOAD_RANDOM_KONACHAN);
		}
		else{
			console.error("Erro Konachan!");
			console.error(response);
		}
	});
}

function sendImageMessage(recipientId, image_url, type_payload) {
  	var messageData = {
    	recipient: { id: recipientId },
	    message: {
	      	attachment: {
	        	type: "image",
	        	payload: { url: image_url },
          	}
        }
    };
	
	callSendAPI(messageData);

	sendButtons(recipientId, image_url, type_payload);
}

function helpMessage(recipientId){
	const message = 'O propósito desse bot é de '+
					'mandar imagens aleatórias a partir de respectivos serviços '+
					'a escolha. Os disponíveis são:\n'+
					'Imgur: imagens de tema aleatório e memes no geral\n'+
					'XKCD: Tirinhas de humor de informática\n'+
					'Konachan: Imagens de tema anime e jogos no geral\n'+
					'Para utilizar o bot, ao iniciar o chat simplesmente mande uma '+
					'mensagem qualquer para o menu aparecer. A partir dele, '+
					'você pode selecionar com a seta ao lado direito do '+
					'qual serviço você deseja que o bot pegue uma imagem.';

	const message2 = 'O bot também enviará também três botões de ação: \n' +
					'Link original: Abre a fonte da imagem\n'+
					'Mais!: pega outra imagem da mesma fonte que você escolheu.\n'+
					'Menu: chama o menu principal novamente.';
	
	sendTextMessage(recipientId, message);
	sendTextMessage(recipientId, message2); // Não é exatamente adequado mandar dois seguidos, mas funciona.
}

function sendTextMessage(recipientId, messageText) {
	var messageData = {
		recipient: {
	  		id: recipientId
		},
		message: {
	  		text: messageText
		}
	};

	callSendAPI(messageData);
}

// Template do Menu
function sendTemplate(recipientId){
	var templateData = {
		recipient: { id: recipientId },
		message: {
			attachment: {
				type: "template",
				payload: {
					template_type: "generic",
					elements: 
					[{
						title: "2IBot",
						subtitle: "Bem vindo jovem, sou sua bot para imagens aleatórias!",
						image_url: "https://my.mixtape.moe/whdyra.png",
						buttons:
						[{
							type: "postback",
							title: "Como usar!",
							payload: PAYLOAD_HELP
						}]
					},
					{
						title: "imgur",
						subtitle:"Site de imagens muito popular, para todos os gostos, inclusive memes!",
						image_url: "http://s.imgur.com/images/logo-1200-630.jpg?2",
						buttons:
						[{
							type: "postback",
							title: "Aleatória!",
							payload: PAYLOAD_RANDOM_IMGUR
						},
						{
							type: "postback",
							title: "MEMES!",
							payload: PAYLOAD_MEME_IMGUR
						}]
					},
					{
						title: "XKCD",
						subtitle: "Popular site para tirinhas sobre informática. Engraçado se você é da área, claro.",
						image_url: "http://www.userlogos.org/files/logos/euphonicnight/xkcdwR.png",
						buttons:
						[{
							type: "postback",
							title: "Tirinhas!",
							payload: PAYLOAD_RANDOM_XKCD
						}]
					},
					{
						title: "Konachan.net",
						subtitle: "Banco de imagens de animes. Para seu weeaboo interior...",
						image_url: "https://my.mixtape.moe/xovpjt.png",
						buttons:
						[{
							type: "postback",
							title: "MOE IS LIFE!",
							payload: PAYLOAD_RANDOM_KONACHAN
						}]
					}]
				}
			}
		}

	};

	callSendAPI(templateData);
}

function sendButtons(recipientId, image_url, type_payload){
	var buttonsData = {
		recipient: { id: recipientId },
		message: {
			attachment: {
				type: "template",
				payload: {
					template_type: "button",
					text: "O que deseja fazer agora?",
					buttons:
					[{
						type: "web_url",
						title: "Link original",
						url: image_url
					},
					{
						type: "postback",
						title: "Mais!",
						payload: type_payload
					},
					{
						type: "postback",
						title: "Menu",
						payload: PAYLOAD_MENU
					}]
				}
			}
		}
	};

	callSendAPI(buttonsData);
}

function callSendAPI(messageData) {
	request({
		uri: 'https://graph.facebook.com/v2.6/me/messages',
		qs: { access_token: 'EAAbrJiWWSmoBAOkiBy7EOaFibrjtF0kl76gU3hNBBc5ZBiGpad7ZAM4nhMRk5LU5KDvZBq5tAJDwjBfP7DlprVSjZCI7kJzBRPD5RDR4y6Y9CuFIcdbMD7EFqZCIEu8uLm7EQTlsRUsAE6koJ6kLOTWlL7CNRkZC1bZChcUZBs6O4pTdvDbgD6Sh' },
		method: 'POST',
		json: messageData

	}, 
	function (error, response, body) {
		if (!error && response.statusCode == 200) {
		  	console.log("callSendAPI(): sucesso!");

		} 
		else {
	  		console.error("Impossível de enviar mensagem");
	  		console.error(response);
	  		console.error(error);
		}
	});  
}